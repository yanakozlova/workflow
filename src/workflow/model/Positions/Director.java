package workflow.model.Positions;

import workflow.model.Person;
import workflow.generators.EmployeeGenerator;
import workflow.generators.TaskGenerator;
import workflow.model.EmployeeWithFixedRate;

import java.util.Map;
import java.util.Set;

/**
 * Created by Yana Kozlova on 17.01.2017.
 */
public class Director extends APosition implements EmployeeWithFixedRate {
    private static double fixedRate;

    public static void toWork(Map<Person, Set<enumPosition>> positionMap) {
        int countOfTask = 1 + (int) (Math.random() * 9);//количество задач на час
        // раздача задач
        for (int i = 0; i < countOfTask; i++){
            EmployeeGenerator.personMap.putAll(TaskGenerator.setTaskToPerson(TaskGenerator.TaskGenerate(TaskGenerator.taskList), EmployeeGenerator.personMap));
        }
        whoIsFree(EmployeeGenerator.personMap);//кто занят через час работы
        whoIsFree(EmployeeGenerator.freeLancersMap);
        whoIsOnWork(EmployeeGenerator.personMap);// кто на работе
    }

    //кто занят через час работы
    private static void whoIsFree(Map<Person, Set<enumPosition>> personMap) {
        for (Map.Entry<Person, Set<enumPosition>> entry : personMap.entrySet())
            entry.getKey().whoIsFree();
    }

    // кто на работе
    private static void whoIsOnWork(Map<Person, Set<enumPosition>> personMap) {
        for (Map.Entry<Person, Set<enumPosition>> entry : personMap.entrySet())
            entry.getKey().whoIsOnWork();

    }

    public  double getFixedRate() {
        return fixedRate;
    }

    public void setFixedRate(double fixedRate) {
        Director.fixedRate = fixedRate;
    }
}

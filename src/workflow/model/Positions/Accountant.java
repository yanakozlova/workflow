package workflow.model.Positions;

import workflow.model.EmployeeWithFixedRate;
import workflow.model.Person;

import java.util.Map;
import java.util.Set;

import static workflow.generators.EmployeeGenerator.freeLancersMap;
import static workflow.generators.EmployeeGenerator.personMap;

/**
 * Created by Yana Kozlova on 17.01.2017.
 */
public class Accountant extends APosition implements EmployeeWithFixedRate {
    double fixedRate;


    public static void toWork() {
        payWeekSalary();
        allSalary();
    }

    // выплачиваем недельную зарплату
    private static void payWeekSalary() {
        for (Map.Entry<Person, Set<enumPosition>> entry : personMap.entrySet()) {
            Person p = entry.getKey();
            p.weekSalary = p.FixedRate / 4 + p.HourRate * p.workedHoursPerWeek;
            p.allWorkedHours += p.workedHoursPerWeek;
            p.countAllTasks += p.countTasks;
        }
        for (Map.Entry<Person, Set<enumPosition>> entry : freeLancersMap.entrySet()) {
            Person p = entry.getKey();
            p.weekSalary = p.FixedRate / 4 + p.HourRate * p.workedHoursPerWeek;
            p.allWorkedHours += p.workedHoursPerWeek;
            p.countAllTasks += p.countTasks;
        }
    }

    // Выплачиваем месячную зарплату
    private static void allSalary() {
        for (Map.Entry<Person, Set<enumPosition>> entry : personMap.entrySet()) {
            Person p = entry.getKey();
            p.MonthSalary = p.FixedRate + p.allWorkedHours * p.HourRate;
        }
        for (Map.Entry<Person, Set<enumPosition>> entry : freeLancersMap.entrySet()) {
            Person p = entry.getKey();
            p.MonthSalary = p.FixedRate + p.allWorkedHours * p.HourRate;
        }
    }
}


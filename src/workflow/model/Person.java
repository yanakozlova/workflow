package workflow.model;

import workflow.model.Positions.APosition;
import workflow.model.Positions.enumPosition;

import java.util.Map;

/**
 * Created by Yana Kozlova on 18.01.2017.
 */
public class Person {
    public String name;
    public Boolean isBusy = false;
    public Boolean onWork = true;
    public int hourPerTask = 0;
    public int workSchedulePerWeek;
    public int allWorkedHours = 0;
    public int workedHoursPerWeek = 0;
    public int countTasks = 0;
    public int countAllTasks = 0;
    public double weekSalary = 0;
    public double MonthSalary = 0;
    public double HourRate = 0;
    public double FixedRate = 0;
    private Map<enumPosition, APosition> listPositions;

    public Person(String s) {
        this.name = s;
    }

    public void getToWork(int hourPerTask) {
        this.hourPerTask = hourPerTask;
        countTasks++;
        workedHoursPerWeek++;
        if (this.hourPerTask == 1) isBusy = true;
        else isBusy = false;
    }

    //кто занят через час работы
    public void whoIsFree() {
        this.hourPerTask--;
        if (hourPerTask == 1) {
            isBusy = true;
            workedHoursPerWeek++;
        } else isBusy = false;

    }

    // кто на работе
    public void whoIsOnWork() {
        if (workSchedulePerWeek - workedHoursPerWeek <= 0) {
            onWork = false;
        }
    }


    public void setListPositions(Map<enumPosition, APosition> listPositions) {

        this.listPositions = listPositions;
    }


}

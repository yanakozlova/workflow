package workflow;


import workflow.generators.EmployeeGenerator;
import workflow.generators.ReportGenerator;
import workflow.model.Person;
import workflow.model.Positions.Accountant;
import workflow.model.Positions.Director;
import workflow.model.Positions.enumPosition;

import java.util.Map;
import java.util.Set;

/**
 * Created by Yana Kozlova on 22.01.2017.
 */
public class Workflow {

    public static void main(String[] args) {
        // генерируем работников
        EmployeeGenerator.generateEmployees();
        //находим директора и время его работы
        int workSchedule = 0; // количество рабочего время директора на неделю
        for (Map.Entry<Person, Set<enumPosition>> entry : EmployeeGenerator.personMap.entrySet()) {
            if (entry.getValue().contains(enumPosition.Director)) {
                workSchedule = entry.getKey().workSchedulePerWeek;
                entry.getKey().workedHoursPerWeek = workSchedule;
            }
        }
        for (int week = 0; week < 4; week++) {
            for (int i = 0; i <= workSchedule; i++)
                Director.toWork(EmployeeGenerator.personMap);
            Accountant.toWork();
        }

        // генерируем отчеты
        ReportGenerator.generalReport();
        ReportGenerator.employeeParticularReport();
        ReportGenerator.freelancerParticularReport();
    }


}

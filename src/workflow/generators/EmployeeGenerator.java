package workflow.generators;

import workflow.model.Person;
import workflow.model.Positions.*;

import java.util.*;

/**
 * Created by Yana Kozlova on 18.01.2017.
 */
public class EmployeeGenerator {
    static final Random RANDOM = new Random();
    private static final int maxNumberOfPersonal = 100;
    private static final int minNumberOfPersonal = 10;
    private static final int minWorkHourPerWeek = 20;
    private static final int maxWorkHourPerWeek = 40;
    private static final double minHourlyRate = 7.5;
    private static final double maxHourlyRate = 15;
    private static final double maxFixedRate = 4000;
    private static final double minFixedRate = 1000;
    //счетчики
    public static int numberOfDirectors = 0;
    public static int numberOfManagers = 0;
    public static int numberOfAccountants = 0;
    public static int numberOfDesigners = 0;
    public static int numberOfProgrammers = 0;
    public static int numberOfTesters = 0;
    static int numberOfFreeLancers = 0;
    //
    public static Map<Person, Set<enumPosition>> freeLancersMap = new HashMap<>();
    public static Map<Person, Set<enumPosition>> personMap = new HashMap<>();

    private static final List<enumPosition> positionValues = Collections.unmodifiableList(Arrays.asList(enumPosition.values()));
    private static final int countPosition = positionValues.size();
    static final int numberOfPersonal = minNumberOfPersonal + (int) (Math.random() * ((maxNumberOfPersonal - minNumberOfPersonal) + 1));

    //---------------------------------------
    // генератор набора случайных должностей
    private static Set<enumPosition> setRandomPositions() {
        Set<enumPosition> list = new HashSet<>();
        //Обязательные должности
        if (EmployeeGenerator.numberOfDirectors < 1) {
            list.add(enumPosition.Director);
            EmployeeGenerator.numberOfDirectors++;
            return list;
        } else if (EmployeeGenerator.numberOfAccountants < 1) {
            list.add(enumPosition.Accountant);
            EmployeeGenerator.numberOfAccountants++;
            return list;
        } else if (EmployeeGenerator.numberOfManagers < 1) {
            list.add(enumPosition.Manager);
            EmployeeGenerator.numberOfManagers++;
            return list;
        } else {
            // остальные + Менеджер и Бухгалтер
            int amountPositions = RANDOM.nextInt(1) + 1; //количество должностей
            for (int i = 0; i <= amountPositions; i++) {
                int x = RANDOM.nextInt(countPosition); //выбор случайной должности
                switch (enumPosition.values()[x]) {

                    case Director:
                        list.add(enumPosition.values()[x]);//добавление в список должность}
                        EmployeeGenerator.numberOfDirectors++;
                        break;
                    case Accountant:
                        list.add(enumPosition.values()[x]);//добавление в список должность}
                        EmployeeGenerator.numberOfAccountants++;
                        break;
                    case Manager:
                        list.add(enumPosition.values()[x]);//добавление в список должность}
                        EmployeeGenerator.numberOfManagers++;
                        break;
                    case Designer:
                        list.add(enumPosition.values()[x]);//добавление в список должность}
                        EmployeeGenerator.numberOfDesigners++;
                        break;
                    case Programmer:
                        list.add(enumPosition.values()[x]);//добавление в список должность}
                        EmployeeGenerator.numberOfProgrammers++;
                        break;
                    case Tester:
                        list.add(enumPosition.values()[x]);//добавление в список должность}
                        EmployeeGenerator.numberOfTesters++;
                        break;

                }
            }
            return list;
        }
    }

    // генератор сотрудников
    private static Person generateOnePerson(int i) {
        Person person = new Person("Сотрудник №" + i);
        person.workSchedulePerWeek = minWorkHourPerWeek + (int) (Math.random() * ((maxWorkHourPerWeek - minWorkHourPerWeek) + 1));
        return person;
    }

    // генератор должностей сотрудников
    private static Map<enumPosition, APosition> createPositionsForPerson(Set<enumPosition> posList) {
        Map<enumPosition, APosition> positionMap = new HashMap<>();
        for (enumPosition pos : posList) {
            int countPos = pos.ordinal();
            switch (countPos) {
                case 0:
                    positionMap.put(pos, new Programmer());
                    break;
                case 1:
                    positionMap.put(pos, new Designer());
                    break;
                case 2:
                    positionMap.put(pos, new Tester());
                    break;
                case 3:
                    positionMap.put(pos, new Manager());
                    break;
                case 4:
                    positionMap.put(pos, new Director());
                    break;
                case 5:
                    positionMap.put(pos, new Accountant());
                    break;
                default:
                    break;
            }
        }
        return positionMap;
    }

    //-------------------------------------------
    // генерация сотрудников
    public static void generateEmployees() {
        for (int i = 0; i < numberOfPersonal; i++) {
            Set<enumPosition> positionList = setRandomPositions();
            Person person1 = generateOnePerson(i);
            person1.setListPositions(createPositionsForPerson(positionList));
            personMap.put(person1, positionList);
            setRates(personMap);
        }

    }

    // генерация фрилансеров
    static void createFreeLancer(int hourPerTask, enumPosition position) {
        Set<enumPosition> positionList = new HashSet<>();
        positionList.add(position);
        Person freeLancer = new Person("Фрилансер №" + numberOfFreeLancers++);
        freeLancer.setListPositions(createPositionsForPerson(positionList));
        freeLancer.HourRate = minHourlyRate + Math.random() * ((maxHourlyRate - minHourlyRate) + 1);
        freeLancer.hourPerTask = hourPerTask;
        freeLancer.weekSalary = freeLancer.HourRate * hourPerTask;
        freeLancersMap.put(freeLancer, positionList);
    }

    // устанавливаем оплату
    private static void setRates(Map<Person, Set<enumPosition>> personMap) {
        for (Map.Entry<Person, Set<enumPosition>> entry : personMap.entrySet()) {
            Set<enumPosition> personsPositions = entry.getValue();
            if (personsPositions.contains(enumPosition.Director) || personsPositions.contains(enumPosition.Accountant)
                    || personsPositions.contains(enumPosition.Manager)) {
                entry.getKey().FixedRate = minFixedRate + (int) (Math.random() * ((maxFixedRate - minFixedRate) + 1));
            }
            if (personsPositions.contains(enumPosition.Designer) || personsPositions.contains(enumPosition.Programmer)
                    || personsPositions.contains(enumPosition.Tester)) {
                entry.getKey().HourRate = minHourlyRate + (int) (Math.random() * ((maxHourlyRate - minHourlyRate) + 1));
            }
        }
    }

}

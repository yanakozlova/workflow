package workflow.generators;

import workflow.model.Person;
import workflow.model.Positions.enumPosition;

import java.util.*;

import static workflow.generators.EmployeeGenerator.createFreeLancer;
import static workflow.generators.EmployeeGenerator.freeLancersMap;

/**
 * Created by Yana Kozlova on 19.01.2017.
 */
public class TaskGenerator {
    public static final HashMap<String, enumPosition> taskList = new HashMap();

    static {
        taskList.put("писать код", enumPosition.Programmer);
        taskList.put("рисовать макет", enumPosition.Designer);
        taskList.put("продавать услуги", enumPosition.Manager);
        taskList.put("составить отчетность", enumPosition.Accountant);
        taskList.put("тестировать программу", enumPosition.Tester);
        taskList.put("писать тест-план", enumPosition.Tester);
        taskList.put("составлять баг-репорт", enumPosition.Tester);
        taskList.put("контролировать работу", enumPosition.Director);
    }

    //------------------------------------------------------------
    // генерируем случайную пару Должность=задача
    public static Map<String, enumPosition> TaskGenerate(HashMap<String, enumPosition> taskList) {
        int countOfTask;
        Map<String, enumPosition> randomValue = new HashMap<>();
        List<enumPosition> keySet = new ArrayList<enumPosition>(taskList.values());
        countOfTask = 1 + (int) (Math.random() * 9);
        for (int i = 0; i < countOfTask; i++) {
            int randomIndex = new Random().nextInt(keySet.size());
            enumPosition pos = keySet.get(randomIndex);
            String task = String.valueOf(keySet.get(randomIndex));
            randomValue.put(task, pos);
        }
        return randomValue;
    }

    //------------------------------------------------
    //назначаем задачи сотрудникам
    public static Map<Person, Set<enumPosition>> setTaskToPerson(Map<String, enumPosition> taskList, Map<Person, Set<enumPosition>> personMap) {
        m1:
        for (Map.Entry<String, enumPosition> task : taskList.entrySet()) {
            //Количество часов на задание от 1 до 2
            int hourPerTask = EmployeeGenerator.RANDOM.nextInt(2) + 1;
            for (Map.Entry<Person, Set<enumPosition>> entry : personMap.entrySet()) {
                //выбираем должности
                Set<enumPosition> personsPositions = entry.getValue();
                if (personsPositions.contains(task.getValue())) {
                    //если сотрудник не занят и на работе
                    if ((entry.getKey().isBusy.equals(false)) && (entry.getKey().onWork.equals(true))) {
                        //назначаем сотруднику задание - сотрудник приступает к работе
                        entry.getKey().getToWork(hourPerTask);
                        continue m1;
                    }
                }
            }
            // если все сотрудники заняты ищем среди уже сотрудничающих фрилансеров
            searchInFreeLancerMap(freeLancersMap, task.getValue(), hourPerTask);
        }
        return personMap;
    }

    private static void searchInFreeLancerMap(Map<Person, Set<enumPosition>> freelancers, enumPosition position, int hourPerTask) {
        Boolean perform = false;
        do {
            for (Map.Entry<Person, Set<enumPosition>> entry : freelancers.entrySet()) {
                Set<enumPosition> personsPositions = entry.getValue();
                if (personsPositions.contains(position)) {
                    if ((entry.getKey().isBusy.equals(false)) && (entry.getKey().onWork.equals(true))) {
                        entry.getKey().getToWork(hourPerTask);
                        perform = true;
                        break;
                    }
                }
            }
            // если нет свободного из сотрудничающих фрилансеров, создаем нового
            if (!perform) {
                createFreeLancer(hourPerTask, position);
            }
        }
        while (!perform);
    }


}




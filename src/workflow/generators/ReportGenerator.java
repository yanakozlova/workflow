package workflow.generators;

import workflow.model.Person;
import workflow.model.Positions.enumPosition;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Yana Kozlova on 22.01.2017.
 */
public class ReportGenerator {
    public static Map<enumPosition, Integer> taskCounter = new HashMap<>();

    static {
        taskCounter.put(enumPosition.Programmer, 0);
        taskCounter.put(enumPosition.Designer, 0);
        taskCounter.put(enumPosition.Tester, 0);
        taskCounter.put(enumPosition.Manager, 0);
        taskCounter.put(enumPosition.Accountant, 0);
    }

    public static void generalReport() {
        int count = 0;
        for (Map.Entry<Person, Set<enumPosition>> entry : EmployeeGenerator.personMap.entrySet()) {
            if (EmployeeGenerator.personMap.get(entry.getKey()).size() > 1) {
                count++;
            }
        }
        try (FileWriter writer = new FileWriter("generalReport.txt", false)) {
            writer.write(String.format("%40s%n", "Общая информация по рабочим"));
            writer.write("Всего сотрудников:" + String.format("%9s%n", EmployeeGenerator.numberOfPersonal));
            writer.write(String.format("%23s%n", "Из них с должностью :"));
            writer.write(String.format("%17s %9s%n", "Директора :", EmployeeGenerator.numberOfDirectors));
            writer.write(String.format("%20s %6s%n", "Программиста :", EmployeeGenerator.numberOfProgrammers));
            writer.write(String.format("%17s %9s%n", "Дизайнера :", EmployeeGenerator.numberOfDesigners));
            writer.write(String.format("%20s %6s%n", "Тестировщика :", EmployeeGenerator.numberOfTesters));
            writer.write(String.format("%17s %9s%n", "Менеджера :", EmployeeGenerator.numberOfManagers));
            writer.write(String.format("%18s %8s%n", "Бухгалтера :", EmployeeGenerator.numberOfAccountants));
            writer.write(String.format("%s %n","  Занимают более одной должности : " + count));
            writer.write(String.format("%s %13s%n%n","Фрилансеров :" , EmployeeGenerator.numberOfFreeLancers));

            writer.write(String.format("%30s %n","Сумарный отчет"));
            writer.write(String.format("%s %n","   По сотрудникам :"));

            double countPayment = 0;
            int countTask = 0;
            int manHour = 0;
            for (Map.Entry<Person, Set<enumPosition>> entry : EmployeeGenerator.personMap.entrySet()) {
                Person p = entry.getKey();
                countPayment += p.MonthSalary;
                countTask += p.countAllTasks;
                manHour += p.allWorkedHours;
            }
            writer.write(String.format("%s %.2f%n","      Всего выплачено : ", countPayment));
            writer.write(String.format("%s %n","      Всего выполнено заданий : " + countTask));
            writer.write(String.format("%s %n","      Человеко-часов  : " + manHour));
            writer.write(String.format("%s %n","   По фрилансерам :"));
            countPayment = 0;
            countTask = 0;
            manHour = 0;
            for (Map.Entry<Person, Set<enumPosition>> entry : EmployeeGenerator.freeLancersMap.entrySet()) {
                Person p = entry.getKey();
                countPayment += p.MonthSalary;
                countTask += p.countAllTasks;
                manHour += p.allWorkedHours;
            }
            writer.write(String.format("      Всего выплачено : %.2f%n", countPayment));
            writer.write(String.format("%s %n","      Всего выполнено заданий: " + countTask));
            writer.write(String.format("%s %n","      Человеко-часов : " + manHour));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void employeeParticularReport() {
        try (FileWriter writer = new FileWriter("employeeParticularReport.txt", false)) {
            writer.write(String.format("%s%n", "--------------------------------------------------------------------------------------------------"));
            writer.write(String.format("%60s%n", "Подробный отчет по сотрудникам"));
            writer.write(String.format("%s%n", "--------------------------------------------------------------------------------------------------"));
            writer.write(String.format("%s%n", "Табельный номер | График работы(ч/нед) | Отработано(ч/мес) | Выполнено заданий | Зарплата за месяц"));
            for (Map.Entry<Person, Set<enumPosition>> entry : EmployeeGenerator.personMap.entrySet()) {
                Person p = entry.getKey();
                String str = String.format("%-14s | %-20s | %-17s | %-17s | %.2f%n", p.name, p.workSchedulePerWeek, p.allWorkedHours, p.countAllTasks, p.MonthSalary);
                writer.write(str);
            }
            writer.write(String.format("%s%n", "--------------------------------------------------------------------------------------------------"));
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }

    public static void freelancerParticularReport() {

        try (FileWriter writer = new FileWriter("freelancerParticularReport.txt", false)) {
            if (EmployeeGenerator.freeLancersMap.size() > 0) {
                writer.write(String.format("%s%n", "---------------------------------------------------------------------------"));
                writer.write(String.format("%50s%n", "Подробный отчет по фрилансерам"));
                writer.write(String.format("%s%n", "---------------------------------------------------------------------------"));
                writer.write(String.format("%s%n", "Табельный номер | Отработано(ч/мес) | Выполнено заданий | Зарплата за месяц"));
                for (Map.Entry<Person, Set<enumPosition>> entry : EmployeeGenerator.freeLancersMap.entrySet()) {
                    Person p = entry.getKey();
                    String str = String.format("%-14s | %-17s | %-17s | %.2f%n", p.name, p.allWorkedHours, p.countAllTasks, p.MonthSalary);
                    writer.write(str);
                }
                writer.write(String.format("%s%n", "---------------------------------------------------------------------------"));
            } else writer.write("Услугами фрилансеров не пользовались");
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }
}

